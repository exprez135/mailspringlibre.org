# Mailspring Libre

Website for the [Mailspring Libre project](https://github.com/notpushkin/Mailspring-Libre).

### Patches

Send patches to [~exprez135/public-inbox@lists.sr.ht](mailto:~exprez135/public-inbox@lists.sr.ht). Please follow appropriate mailing list etiquette.
