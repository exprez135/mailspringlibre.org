+++
draft= false
title = "FAQ"
description = "Asked and answered"
+++

## Why not the original Mailsping?

Foundry376's Mailspring requires you to have a Mailspring account and relies on their central server. This fork removes those requirements and disables features which invade your privacy, collect data, or don't respect your freedoms.

## What's the plan? 

Mailspring Libre's main goal is to maintain a version of Mailspring which does not depend on any central server. We disable telemetry and parts of the project which don't fit with our philosophy. When possible, we aim to re-create features from upstream which are disabled for their risks (excluding features which are incompatible with our philosophy, like link tracking). We also aim to completely replace *Mailsync*.

If time and resources permit, we may be able to create novel features unique to this fork.

## What about Mailsync? 

Mailsping uses *Mailsync*, a proprietary program which actually syncs your mail. Mailsync comes bundled with the original Mailspring. We hope to replace this with a fully free and open source alternative as soon as possible (want to help? let us know!). Mailsync comes with a [restrictive license](https://github.com/notpushkin/Mailspring-Libre/blob/master/LICENSE-mailsync.md) which prohibits its use with anything other than the original Mailspring, unless you get permission.
