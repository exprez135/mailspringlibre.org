---
title: "Privacy Policy"
date: 2020-12-15T16:06:38-06:00
draft: false 
---

We collect as little data about you as possible. We have no centralized server, no telemetry, and no accounts. 

Mailspring Libre performs email sync locally on your computer and does not transfer mail data (messages, headers, attachments, etc.) or metadata derived from your mail data off of your computer. 

We collect no data from you when you browse our website.

When you authorize Mailspring Libre's access to third-party email providers like Google's Gmail, we receive no information about you. All data is sent to your local Mailspring Libre client. This process is only to enable the app's access to your email accounts.

Mailspring Libre's use of information received from Google APIs adhere to the [Google API Services User Data Policy](https://developers.google.com/terms/api-services-user-data-policy#additional_requirements_for_specific_api_scopes), including the Limited Use requirements. 
