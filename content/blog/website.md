+++
title = "New Mailspring Libre site!"
description = ""
date = 2020-12-15T15:38:17-06:00
weight = 20
draft = false
+++

The Mailspring Libre project has a new website at https://mailspringlibre.org.

Source: <https://git.sr.ht/~exprez135/mailspringlibre.org>

Patches: Send patches to [~exprez135/public-inbox@lists.sr.ht](mailto:~exprez135/public-inbox@lists.sr.ht).

Over time we will hopefully use the site to enable OAuth access for the email client, provide documentation for developers and users, and more!
